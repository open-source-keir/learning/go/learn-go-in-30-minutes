package main

import "fmt"

func main() {
	// Make two buffered channels
	commands := make(chan int, 100)
	journals := make(chan int, 100)

	// Create multiple concurrent workers as go routines, all receiving messages from the same commands channel
	go worker(commands, journals)
	go worker(commands, journals)
	go worker(commands, journals)
	go worker(commands, journals)

	// Fill up the buffered command channel with numbers 1-100
	for i := 0; i < 100; i++ {
		commands <- i
	}
	// Close commands channel (as the sender, so it's okay to do!)
	close(commands)

	// Receive 100 messages from the journals channel
	for j := 0; j < 100; j++ {
		fmt.Println(<-journals)
	}
}

func worker(commands <-chan int, journals chan<- int) {
	// Receive messages from the commands channel, auto turn off if it's closed
	for n := range commands {
		// Send the result of the work done to the journals channel
		journals <- fibonacci(n)
	}
}

func fibonacci(n int) int {
	if n <= 1 {
		return n
	}
	return fibonacci(n-1) + fibonacci(n-2)
}

