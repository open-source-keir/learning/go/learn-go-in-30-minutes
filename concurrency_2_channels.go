package main

import (
	"fmt"
	"time"
)

func main() {
	// NOTES:
	// - Sending & Receiving Messages From Channels Are Blocking Operations -> Can Synchronise Go Routines!
	// - Only ever close a channel from the sender POV, never from the receiver end!
	// - Can receive a second value on the channel that states if it is open or not.

	// Make a channel that takes string messages
	c := make(chan string)

	// Start a go routine that calls countWithChannel
	go countWithChannel("Sheep", c)

	// METHOD 1 - VERBOSE
	//// Loop through to receive all messages from a channel
	//for {
	//	// Receive a (message, isOpen) tuple from the channel
	//	msg, isOpen := <- c
	//
	//	// If the channel has been closed by the sender, break out of the for loop to stop attempting to receive messages
	//	if !isOpen {
	//		break
	//	}
	//	fmt.Println(msg)
	//}

	// METHOD 2 - SUGAR
	// Loop through all messages in a channel & auto stop listening when the channel is closed
	for msg := range c {
		fmt.Println(msg)
	}

}

func countWithChannel(thing string, c chan string) {
	for i := 0; i <= 5; i++ {
		// Send a message on the channel
		c <- thing
		time.Sleep(time.Millisecond * 500)
	}
	// Close the channel (from the sender POV only!) to avoid deadlock error
	close(c)
}
