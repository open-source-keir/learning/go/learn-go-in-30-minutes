package main

import (
	"fmt"
	"time"
)

func main() {
	// Make two channels that take string messages
	c1 := make(chan string)
	c2 := make(chan string)

	// Go routine that adds a message to c1 every 500ms
	go func() {
		for {
			c1 <- "Every 500ms"
			time.Sleep(time.Millisecond * 500)
		}
	}()
	// Go routine that adds a message to c2 every 2s
	go func() {
		for {
			c2 <- "Every 2s"
			time.Sleep(time.Second * 2)
		}
	}()

	// METHOD 1 - NO SELECT - SLOWER CHANNEL BLOCKS FASTER CHANNEL
	//for {
	//	fmt.Println(<-c1)
	//	fmt.Println(<-c2)
	//}

	// METHOD 2 - SELECT - LISTEN TO MULTIPLE CHANNELS W/O A SLOWER ONE BLOCKING
	for {
		// SELECT, so c2 doesn't block the more frequent c1!
		select {
		case msg1 := <- c1:
			fmt.Println(msg1)
		case msg2 := <- c2:
			fmt.Println(msg2)
		}
	}

}
