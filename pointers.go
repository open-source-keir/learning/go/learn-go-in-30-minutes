package main

import "fmt"

func main() {
	// Set x's value to be 7
	x := 7
	// Print the memory address of the variable x - pointer to x
	fmt.Println(&x)
	// Pass copy of x to the increment function
	incrementCopyOfValue(x)
	// Print x - see it's not incremented since function increment was given a COPY of x
	fmt.Println(x)


	// Set y's value to be 7
	y := 7
	// Pass pointer to y to the increment function
	incrementValueAtMemory(&y)
	// Print incremented value of y
	fmt.Println(y)
}

func incrementCopyOfValue(x int) {
	x++
}

func incrementValueAtMemory(y *int) {
	// Retrieve whats at the memory address by de-referencing it using the *y++ (instead of y++ for memory address only)
	*y++
}
