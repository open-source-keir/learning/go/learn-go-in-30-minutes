package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	var wg sync.WaitGroup // Initialise syncWaitGroup with zero value
	fmt.Println(wg)
	wg.Add(2) // Increment the wait group counter by delta -> eg/ there is delta Go routines to wait for
	fmt.Println(wg)

	// Wrap the call to 'go count("Sheep")' in an anonymous function that is immediately invoked
	//go count("Sheep")
	go func() {
		count("Sheep")
		wg.Done() // Decrement the wait group counter by 1
	}()

	go func() {
		count("Cow")
		wg.Done() // Decrement the wait group counter by 1
	}()

	wg.Wait() // Block main go routine until the wait group counter is 0
}

func count(thing string) {
	for i := 0; i <= 5; i++ {
		fmt.Println(i, thing)
		time.Sleep(time.Millisecond * 500)
	}
}
