package main

import "fmt"

func main() {
	// Only loops in Go are for loops

	// For Loop
	fmt.Println("For Loop...")
	for i := 0; i < 5; i++ {
		fmt.Println(i)
	}
	// While Loop
	fmt.Println("While Loop equivalent... ")
	i := 0
	for i < 5 {
		fmt.Println(i)
		i++
	}

	// Iterate over a collection - array
	fmt.Println("Iterate over a collection - array")
	arr := []string{"Keir", "David", "Ryan", "Richie"}
	for index, value := range arr {
		fmt.Println(index, value)
	}

	// Iterate over a collection - map
	fmt.Println("Iterate over a collection - map")
	m := make(map[string]string)
	m["Keir"] = "cool"
	m["Zach"] = "strongest kafka dev in the world"

	for key, value := range m {
		fmt.Println("Key:", key, "Value:", value)
	}
}
