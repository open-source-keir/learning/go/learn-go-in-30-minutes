package main

import "fmt"

func main() {
	// METHOD 1 - NO BUFFER W/ JUST MAIN GO ROUTINE -> INDUCES DEADLOCK
	//// Make a regular, non buffered channel
	//c := make(chan string)
	//// BLOCKING OPERATION - Send a string to the channel
	//c <- "Hello"
	//// Receive operation (would be blocking, too) which is blocked by the send above (since it's same go routine)
	//msg := <- c
	//fmt.Println(msg)

	// METHOD 2 - BUFFERED CHANNEL -> NO DEADLOCK UNTIL BUFFER IS EXCEEDED
	cBuffer := make(chan string, 2)
	cBuffer <- "Hello..."
	cBuffer <- "World!"

	msg := <- cBuffer
	fmt.Println(msg)

	msg = <- cBuffer
	fmt.Println(msg)
}
