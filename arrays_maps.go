package main

import "fmt"

func main() {
	// Initialise a fixed length (5) array with int zero values
	var x [5]int
	fmt.Println(x)

	// Initialise a fixed length (5) array
	y := [5]int{1, 2, 3, 4, 5}
	fmt.Println(y)

	// Initialise a slice of ints
	z := []int{5, 4, 3, 2, 1}
	// Return a new array with an additional element
	z = append(z, 0)
	fmt.Println(z)


	// Initialise a map[key]value
	vertices := make(map[string]int)

	// Set key-value pairs
	vertices["triangle"] = 2
	vertices["square"] = 3
	vertices["dodecagon"] = 12

	fmt.Println(vertices)
	fmt.Println(vertices["dodecagon"])

	// Delete key-value pair
	delete(vertices, "square")
	fmt.Println(vertices)
}
