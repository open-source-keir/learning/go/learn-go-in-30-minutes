package main

import "fmt"

type person struct {
	name string
	age int
	dimensions dimension // struct field can be another struct
}

type dimension struct {
	maxHeight float32
	maxWidth float32
}

func main() {
	p := person{name: "Keir", age: 24, dimensions: dimension{maxHeight: 100.5, maxWidth: 50.0}}
	fmt.Println(p.name)
	fmt.Println(p)
}
