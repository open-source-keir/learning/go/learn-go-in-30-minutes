package main

import (
	"errors"
	"fmt"
	"math"
)

func main() {
	// Print sum using sum function
	fmt.Println(sum(2, 3))

	// Print error if not nil, else print result
	result, err := sqrt(-16)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(result)
	}
}

// Function called sum that takes two integers (x, y) and returns an integer
func sum(x int, y int) int {
	return x + y
}

// Function called sqrt that takes a single float64 and returns multiple values as a tuple (float64, error)
func sqrt(x float64) (float64, error) {
	// Limit function to +ve inputs only

	// Return 0 & error if input is not positive
	if x < 0 {
		return 0, errors.New("square root is undefined for negative numbers")
	}
	// Else return the square root and nil
	return math.Sqrt(x), nil
}
